﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astroid : MonoBehaviour
{
    #region Fields
    public Sprite RandomSprite;
    private List<Sprite> Astroids = new List<Sprite>();
    private Vector2 position;
    private Vector2 newPosition;
    private CircleCollider2D AstroidCollider;
    public float AstroidRadius;
    const float MinImpulseForce = 2f;
    const float MaxImpulseForce = 6f;
    private float angle;
    private Rigidbody2D rb;

    [SerializeField]
    Sprite SmolAstroid;
    [SerializeField]
    Sprite MediumAstroid;
    [SerializeField]
    Sprite LargeAstroid;
    #endregion


    public void Initialize (Direction direction, Vector3 WorldLocation)
    {

        //get rigid body and set up angles
        Rigidbody2D rbd2 = GetComponent<Rigidbody2D>();
        float randomAngle = Random.Range(0, 30);
        float converted = randomAngle * Mathf.Rad2Deg;

        //figure out direction based on enum direction and random angle
        //the angels I'm providing here may not me entirely right, I am unsure why sometimes my astroid decides to go the oppsite direction

        if (direction == Direction.Up)
        {
            angle = converted + 75;
            Debug.Log("Condition UP");
           

        }
        else if (direction == Direction.Left)
        {
            angle = converted + -15;
            Debug.Log("Condition Left");
           
        }
        else if (direction == Direction.Down)
        {
            angle = converted + 265;
            Debug.Log("Condition Down");
           
        }
        else if (direction == Direction.Right)
        {
            angle = converted + 175;
            Debug.Log("Condition Right");
           
        }

        //apply force to rigid body based on previous calculation

        float magnitude = Random.Range(MinImpulseForce, MaxImpulseForce);
        Vector2 moveDirection = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
        GetComponent<Rigidbody2D>().AddForce(moveDirection * magnitude, ForceMode2D.Impulse);
        // added rotation to asteroids for COOL effect
        GetComponent<Rigidbody2D>().AddTorque(magnitude, ForceMode2D.Impulse);

    }

    // Start is called before the first frame update
    void Start()
    {
        // load up the sprites into list
        Astroids.Add(SmolAstroid);
        Astroids.Add(MediumAstroid);
        Astroids.Add(LargeAstroid);
        //get one random sprite
        int index = Random.Range(0, Astroids.Count);
        RandomSprite = Astroids[index];
        //apply sprite on renderer
        Debug.Log("Render!");

        GetComponent<SpriteRenderer>().sprite = RandomSprite;

        //get astroid collider
        AstroidCollider = GetComponent<CircleCollider2D>();
        AstroidRadius = AstroidCollider.radius;

    }

    void OnBecameInvisible()
    {
        position = transform.position;
        // wrap the ship around the screen once it leaves it in a certain direction
        // print new position in log just to make sure my conditions are working correctly
        if (position.x - AstroidRadius > ScreenUtils.ScreenLeft || position.x + AstroidRadius < ScreenUtils.ScreenRight)
        {
            newPosition.x = -position.x;
        }
        if (position.y - AstroidRadius < ScreenUtils.ScreenTop || position.y + AstroidRadius > ScreenUtils.ScreenBottom)
        {
            newPosition.y = -position.y;
        }
        transform.position = newPosition;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // destroy astroids on collision with bullets
        if (collision.gameObject.tag == "Bullet")
        {
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }

    public float GetAstroidRad()
    {
        return AstroidRadius;
    }


}
