﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    /// Bullet Class to handle Force and other stuffs

    #region Fields
    private Rigidbody2D BulletRB;
    private const float TimeAlive = 2f;
    private Timer deathTimer;
    private GameObject bullet;
    private Vector2 position;
    private Vector2 newPosition;
    private CircleCollider2D BulletCollider;
    private float BulletRadius;
    #endregion

    // making sure I got what I need on awake for the bullet
    private void Awake()
    {
        BulletRB = this.GetComponent<Rigidbody2D>();
        BulletCollider = GetComponent<CircleCollider2D>();
        BulletRadius = BulletCollider.radius;

    }
    // death timer initialization on start
    private void Start()
    {
        deathTimer = gameObject.AddComponent<Timer>();
        deathTimer.Duration = TimeAlive;
        deathTimer.Run();
    }
    // time out bullets when deathtimer expires.
    private void Update()
    {
        if(deathTimer.Finished)
        {
            Destroy(gameObject);
        }
    }
    // I should make magnitude a serialized field
    public void ApplyForce (Vector2 Direction)
    {
        const float magnitude = 10f;
       BulletRB.AddForce(magnitude * Direction, ForceMode2D.Impulse);

    }

    //it would be best if I can make wrapping a seperate script instead of copypasta!!!
    void OnBecameInvisible()
    {
        position = transform.position;
        // wrap the ship around the screen once it leaves it in a certain direction
        // print new position in log just to make sure my conditions are working correctly
        if (position.x - BulletRadius > ScreenUtils.ScreenLeft || position.x + BulletRadius < ScreenUtils.ScreenRight)
        {
            newPosition.x = -position.x;
            Debug.Log("position Horzontal" + newPosition);
        }
        if (position.y - BulletRadius < ScreenUtils.ScreenTop || position.y + BulletRadius > ScreenUtils.ScreenBottom)
        {
            newPosition.y = -position.y;
            Debug.Log("position Vertical" + newPosition);
        }
        transform.position = newPosition;

    }
}
