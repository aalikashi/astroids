﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this class is for spawning astroids
// I know it looks rough and there are many many ways to make this look better
// but the class I'm taking has specific instructions on how this can be implemented.

public class AstroidSpawner : MonoBehaviour
{
    #region Fields
    [SerializeField]
    GameObject prefabAsteroid;
    private Vector3 LocationTop;
    private Vector3 LocationBottom;
    private Vector3 LocationLeft;
    private Vector3 LocationRight;
    public GameObject astroidTop;
    public GameObject astroidBottom;
    public GameObject astroidLeft;
    public GameObject astroidRight;

    #endregion
    void Start()
    {
        // instatiate game objects from prefab
        GameObject astroidTop = Instantiate(prefabAsteroid) as GameObject;
        GameObject astroidBottom = Instantiate(prefabAsteroid) as GameObject;
        GameObject astroidLeft = Instantiate(prefabAsteroid) as GameObject;
        GameObject astroidRight = Instantiate(prefabAsteroid) as GameObject;
        
        // figure out location on screen

        Vector3 LocationTop = new Vector3(0, ScreenUtils.ScreenTop, 0);
        Vector3 LocationBottom = new Vector3(0, ScreenUtils.ScreenBottom, 0);
        Vector3 LocationLeft = new Vector3(ScreenUtils.ScreenLeft, 0, 0);
        Vector3 LocationRight = new Vector3(ScreenUtils.ScreenRight, 0,0);
        
        //offset the location nased on astroid rad so it appears fully onscreen

        LocationTop.y -= astroidTop.GetComponent<Astroid>().GetAstroidRad();
        LocationBottom.y += astroidBottom.GetComponent<Astroid>().GetAstroidRad();
        LocationLeft.x += astroidLeft.GetComponent<Astroid>().GetAstroidRad();
        LocationRight.x -= astroidRight.GetComponent<Astroid>().GetAstroidRad();

        //initalize the asteroids and supply them with needed arguments

        astroidTop.GetComponent<Astroid>().Initialize(Direction.Down, LocationTop);
        astroidBottom.GetComponent<Astroid>().Initialize(Direction.Up, LocationBottom);
        astroidLeft.GetComponent<Astroid>().Initialize(Direction.Left, LocationRight);
        astroidRight.GetComponent<Astroid>().Initialize(Direction.Right, LocationLeft);

        //make sure asteroids spawn in the correct location

        astroidTop.transform.position = LocationTop;
        astroidBottom.transform.position = LocationBottom;
        astroidLeft.transform.position = LocationRight;
        astroidRight.transform.position = LocationLeft;

    }


}
