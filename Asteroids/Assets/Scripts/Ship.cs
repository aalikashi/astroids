﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    // Start is called before the first frame update
    //our fighter ship class
    // setting up some variables
    private Rigidbody2D ShipRB;
    public const int thrustForce = 20;
    private Vector2 thrustDirection = new Vector2(1, 0);
    private CircleCollider2D ShipCollider;
    private float ShipRadius;
    private Vector2 position;
    private Vector2 newPosition;
    private const int RotateDegreesPerSecond = 100;
    private float rotationInput;

    [SerializeField]
    GameObject prefabBullet;

    void Start()
    {
        // set some values here and make sure ship always start in middle when game starts

        ShipRB = GetComponent<Rigidbody2D>();
        ShipCollider = GetComponent<CircleCollider2D>();
        ShipRadius = ShipCollider.radius;
        transform.position = Vector3.zero;

    }

    // Update is called once per frame
    void Update()
    {
        position = transform.position;

        // calculate rotation amount and apply rotation
        // calculate direction by using sin cos

        if (Input.GetAxis("Rotation") != 0)
        {
            rotationInput = Input.GetAxis("Rotation");
            float rotationAmount = RotateDegreesPerSecond * Time.deltaTime;
            if (rotationInput < 0) { rotationAmount *= -1; }
            transform.Rotate(Vector3.forward, rotationAmount);
            Vector3 direction = transform.eulerAngles;
            thrustDirection.x = Mathf.Cos(direction.z * Mathf.Deg2Rad);
            thrustDirection.y = Mathf.Sin(direction.z * Mathf.Deg2Rad);



        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            
            GameObject Bullet = Instantiate(prefabBullet) as GameObject;
            Bullet Script = Bullet.GetComponent<Bullet>();
            Bullet.transform.position = transform.position;
            Script.ApplyForce(thrustDirection);

        }

      
    }
    void OnCollisionEnter2D (Collision2D col)
    {
        if (col.gameObject.tag == "Asteroid")
        {
            Debug.Log("SHIP DIED !!!!");
            Destroy(gameObject);
        }
    }
    // this method will be called on keypress to thrust ship
    void FixedUpdate()
    {
        if (Input.GetAxis("Thrust") != 0)
        {
            ShipRB.AddForce(thrustForce * thrustDirection, ForceMode2D.Force);
        }


    }

    void OnBecameInvisible()
    {
        position = transform.position;
        // wrap the ship around the screen once it leaves it in a certain direction
        // print new position in log just to make sure my conditions are working correctly
        if (position.x - ShipRadius > ScreenUtils.ScreenLeft || position.x + ShipRadius < ScreenUtils.ScreenRight)
        {
            newPosition.x = -position.x;
            Debug.Log("position Horzontal" + newPosition);
        }
        if (position.y - ShipRadius < ScreenUtils.ScreenTop || position.y + ShipRadius > ScreenUtils.ScreenBottom)
        {
            newPosition.y = -position.y;
            Debug.Log("position Vertical" + newPosition);    
        }
        transform.position = newPosition;
        
    }
}
